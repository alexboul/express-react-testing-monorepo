interface TitleProps {
    title?: string;
}

export const DEFAULT_TITLE = 'BONK';

function Title({ title = DEFAULT_TITLE }: TitleProps) {
    return (
        <h1>{title}</h1>
    )
}

export default Title;