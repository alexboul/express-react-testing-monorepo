import { useState } from 'react';
import './App.css';
import Title from './Title';
import ToggleButton from './ToggleButton';

function App() {
  const [count, setCount] = useState(0);

  return (
    <>
      <Title />
      <div className="card">
        <ToggleButton />
        <button onClick={() => setCount((count) => count + 1)}>
          count is {count}
        </button>
        <p>
          Edit <code>src/App.tsx</code> and save to test HMR
        </p>
      </div>
      <p className="read-the-docs">
        Click on the Vite and React logos to learn more
      </p>
    </>
  );
}

export default App;
