import { render, screen } from '@testing-library/react';
import Title, { DEFAULT_TITLE } from './Title';

it('should render the title with the correct text', () => {
    const titleString = 'Titre';
    render(<Title title={titleString} />);
    
    const titleElement = screen.getByRole('heading');
    
    expect(titleElement).toBeInTheDocument();
    expect(titleElement).toHaveTextContent(titleString);
});

it('should render the title with default value if no title text is provided', () => {
    render(<Title />);
    
    const titleElement = screen.getByRole('heading');
    
    expect(titleElement).toBeInTheDocument();
    expect(titleElement).toHaveTextContent(DEFAULT_TITLE);
});