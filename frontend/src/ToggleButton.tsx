import { useState } from 'react';
import './ToggleButton.css';

export default function ToggleButton() {
  const [currentColor, setCurrentColor] = useState('red');
  const [isDisabled, setIsDisabled] = useState(false);
  const nextColor = currentColor === 'red' ? 'blue' : 'red';

  function changeColor() {
    setCurrentColor(nextColor);
  }

  function changeAvailability() {
    setIsDisabled(!isDisabled);
  }

  return (
    <div>
      <button
        className={isDisabled ? 'gray' : currentColor}
        onClick={changeColor}
        disabled={isDisabled}
      >
        Change to {nextColor}
      </button>
      <label>
        <input type="checkbox" onChange={changeAvailability} />
        Disable Button
      </label>
    </div>
  );
}
