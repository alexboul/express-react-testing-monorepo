import { fireEvent, render, screen } from '@testing-library/react';
import ToggleButton from './ToggleButton';

describe('ToggleButton', () => {
  it('renders with initial class "red" and text "Change to blue"', () => {
    render(<ToggleButton />);

    const button = screen.getByRole('button', { name: /change to blue/i });

    expect(button).toHaveClass('red');
    expect(button).toHaveTextContent(/change to blue/i);
  });

  it('changes class to "blue" and text to "Change to red" when clicked', () => {
    render(<ToggleButton />);

    const button = screen.getByRole('button', { name: /change to blue/i });

    fireEvent.click(button);

    expect(button).toHaveClass('blue');
    expect(button).toHaveTextContent(/change to red/i);
  });

  it('disables the button when the checkbox is checked', () => {
    render(<ToggleButton />);

    const checkbox = screen.getByRole('checkbox');
    const button = screen.getByRole('button');

    fireEvent.click(checkbox);

    expect(button).toBeDisabled();
  });

  it('enables the button when the checkbox is unchecked', () => {
    render(<ToggleButton />);

    const checkbox = screen.getByRole('checkbox');
    const button = screen.getByRole('button');

    fireEvent.click(checkbox);
    fireEvent.click(checkbox);

    expect(button).not.toBeDisabled();
  });
});
