import React, { useState } from 'react';
import AlertBox from './AlertBox';

interface ValidationOutcome {
  isValid: boolean;
  message: string;
}

/**
 * Vérifie qu'un username est valide : commence par une lettre, suivie de lettres et nombres
 */
function checkUsername(username: string): ValidationOutcome {
  return {
    isValid: /^[a-zA-Z][a-zA-Z0-9]*$/.test(username),
    message: 'Invalid username',
  };
}

/**
 * Vérifie qu'un email est valide (possibilité d'utiliser une lib externe ou une regex)
 */
function checkEmail(email: string): ValidationOutcome {
  const isValid = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/.test(email);
  const message = isValid ? '' : 'Invalid email';
  const outcome = { isValid, message };

  return outcome;
}

/**
 * Vérifie qu'un mot de passe est valide :
 * - au moins 8 caractères,
 * - éventuellement vérifier présence de minuscules, majuscules ET nombres
 */
function checkPassword(password: string): ValidationOutcome {
  const isValid = password.length >= 8;
  const message = isValid ? '' : 'Invalid password: too short (< 8 characters)';
  const outcome = { isValid, message };

  return outcome;
}

export default function SignupForm() {
  const [username, setUsername] = useState('');
  const [usernameValidation, setUsernameValidation] =
    useState<ValidationOutcome | null>(null);

  const [email, setEmail] = useState('');
  const [emailValidation, setEmailValidation] =
    useState<ValidationOutcome | null>(null);

  const [password, setPassword] = useState('');
  const [passwordValidation, setPasswordValidation] =
    useState<ValidationOutcome | null>(null);

  const [submissionOutcome, setSubmissionOutcome] = useState<{
    message: string;
    status: 'success' | 'failure';
  } | null>(null);

  const handleSubmit = async (event: React.FormEvent) => {
    event.preventDefault();
    const usernameError = checkUsername(username);
    const emailError = checkEmail(email);
    const passwordError = checkPassword(password);

    if (
      usernameError.isValid === false ||
      emailError.isValid === false ||
      passwordError.isValid === false
    ) {
      setUsernameValidation(usernameError);
      setEmailValidation(emailError);
      setPasswordValidation(passwordError);
      return;
    }

    setSubmissionOutcome({ message: 'Signed up', status: 'success' });
  };

  return (
    <form onSubmit={handleSubmit}>
      <div>
        <label htmlFor="username">Username:</label>
        <input
          type="text"
          id="username"
          value={username}
          onChange={(e) => setUsername(e.target.value)}
        />
        {usernameValidation?.isValid === false && (
          <AlertBox message={usernameValidation.message} status="failure" />
        )}
      </div>
      <div>
        <label htmlFor="email">Email:</label>
        <input
          type="email"
          id="email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
        {emailValidation?.isValid === false && (
          <AlertBox message={emailValidation.message} status="failure" />
        )}
      </div>
      <div>
        <label htmlFor="password">Password:</label>
        <input
          type="password"
          id="password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
        {passwordValidation?.isValid === false && (
          <AlertBox message={passwordValidation.message} status="failure" />
        )}
      </div>
      <button type="submit">Sign Up</button>
      {submissionOutcome?.status === 'success' && (
        <AlertBox
          message={submissionOutcome.message}
          status={submissionOutcome.status}
        />
      )}
    </form>
  );
}
