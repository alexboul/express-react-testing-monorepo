// src/App.test.tsx
import { render } from '@testing-library/react';
import App from './App';

it('renders without crashing', () => {
   // Rendu du composant App dans le DOM virtuel
  render(<App />);
});
