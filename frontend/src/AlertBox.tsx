interface AlertBoxProps {
  message: string;
  status: string;
}

export default function AlertBox({ message, status }: AlertBoxProps) {
  return (
    <div
      className={
        status === 'success' ? 'green' : status === 'failure' ? 'red' : ''
      }
      role="alert"
    >
      {message}
    </div>
  );
}
