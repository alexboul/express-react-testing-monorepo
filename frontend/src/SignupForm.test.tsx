import { fireEvent, render, screen } from '@testing-library/react';
import SignupForm from './SignupForm';

describe('SignupForm', () => {
  it('should display validation errors for invalid fields on form submission', () => {
    render(<SignupForm />);
    const signUpButton = screen.getByRole('button', { name: /sign up/i });

    fireEvent.click(signUpButton);

    expect(screen.getByText('Invalid username')).toBeInTheDocument();
    expect(screen.getByText('Invalid email')).toBeInTheDocument();
    expect(
      screen.getByText('Invalid password: too short (< 8 characters)')
    ).toBeInTheDocument();
  });

  it('displays a success message after successful form submission', () => {
    render(<SignupForm />);
    const username = screen.getByLabelText(/username/i);
    const email = screen.getByLabelText(/email/i);
    const password = screen.getByLabelText(/password/i);
    const button = screen.getByRole('button', { name: /sign up/i });
    fireEvent.change(username, { target: { value: 'user' } });
    fireEvent.change(email, { target: { value: 'user@ipi.com' } });
    fireEvent.change(password, { target: { value: 'passwordd' } });

    fireEvent.click(button);

    expect(screen.getByText('Signed up')).toBeInTheDocument();
  });
});
