import { render, screen } from '@testing-library/react';
import AlertBox from './AlertBox';

describe('AlertBox', () => {
  it('should display a success message with green color', () => {
    const message = 'Success!';
    const status = 'success';
    render(<AlertBox message={message} status={status} />);

    const alert = screen.getByText(message);

    expect(alert).toHaveClass(status);
  });

  it('should display a failure message with red color', () => {
    const message = 'Failure!';
    const status = 'failure';
    render(<AlertBox message={message} status={status} />);

    const alert = screen.getByText(message);

    expect(alert).toHaveClass(status);
  });
});
